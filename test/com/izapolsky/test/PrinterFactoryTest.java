package com.izapolsky.test;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.junit.Assert.*;

public class PrinterFactoryTest {

    @Test
    public void testTextFormatAvailable() {
        assertNotNull("Should be available", PrinterFactory.printerFor(PrintFormat.TEXT));
    }


    @Test
    public void testTextPrinter() throws IOException {
        Printer textPrinter = PrinterFactory.printerFor(PrintFormat.TEXT);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();


        Stream.of(new Operation()).map(textPrinter::print).forEach(it -> it.writeTo(baos));

        baos.flush();


        assertEquals(Operation.class.getName(), new String(baos.toByteArray()));

    }
}