package com.izapolsky.test;

public interface Printer {

    Representation print(Element el);
}
