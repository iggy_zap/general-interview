package com.izapolsky.test;

import java.io.IOException;
import java.io.OutputStream;

/**
 * A marker interface for represenation of element after printer done it's job
 */
public interface Representation {

    /**
     * A way to write representation somewhere
     * @param os
     * @throws IOException
     */
    void writeTo(OutputStream os);
}
