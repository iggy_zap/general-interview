package com.izapolsky.test;

import java.io.IOException;
import java.io.OutputStream;

public class PrinterFactory {

    /**
     * Factory method to obtain a printer for now
     *
     * @param pf
     * @return
     */
    public static Printer printerFor(PrintFormat pf) {
        switch (pf) {
            case TEXT:
                return new TextPrinter();
            default:
                throw new UnsupportedOperationException("Not yet supported");
        }
    }


    static class TextPrinter implements Printer {

        /**
         * A simple text printer which just writes out name of the class
         *
         * @param el
         * @return
         */
        @Override
        public Representation print(Element el) {
            return os -> {
                try {
                    os.write(el.getClass().getName().getBytes());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            };
        }
    }
}
