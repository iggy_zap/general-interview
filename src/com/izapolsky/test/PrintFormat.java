package com.izapolsky.test;

/**
 * Enum that represents supported formats
 */
public enum PrintFormat {
    PDF,
    PNG,
    TEXT
}
